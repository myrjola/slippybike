#include <bcm2835.h>
#include <stdio.h>
#include <sys/types.h>
#include "adxl345/ADXL345.h"
#include "slippybike.h"

// Convert the value from accelerometer to G's
float to_g(int16_t accelvalue) {
  return (float) accelvalue * 0.004;
}

// Convert the value from accelerometer to meters/s.
float to_meters_per_second(int16_t accelvalue) {
  return (float) accelvalue * 0.000407888;
}

void *accelerometer_reader(__attribute__ ((unused)) void* param) {
  DEBUG("Initializing accelerometer...\n");
  if (!ADXL345_Init(ADXL345_I2C_COMM)) return 0;

  DEBUG("Reading from accelerometer.\n");
  int16_t x, y, z;
  float a_x, a_y, a_z;

  long long int prev_time = 0, current_time = 0, delta_time = 0;

  while(true) {
    current_time = *g_timer;
    ADXL345_GetXyz(&x, &y, &z);
    a_x = to_meters_per_second(x);
    a_y = to_meters_per_second(y);
    a_z = to_meters_per_second(z);

    // Update velocity based on accelerometer data.
    delta_time = current_time - prev_time;
    g_velocity += a_y * delta_time / 1E6;
    prev_time = current_time;

    DEBUG("%lld\n", delta_time);
    DEBUG("X:%f Y:%f Z:%f\n", a_x, a_y ,a_z);
  }
}
