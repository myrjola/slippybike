/* Copyright (c) 2016 Slippybike team */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>
#include <bcm2835.h>

#include "slippybike.h"

int main(void)
{
  DEBUG("Initializing BCM2835 library...\n");
  if (!bcm2835_init()) return 0;

  // Map the RPi 1MHz timer to a pointer.
  g_timer = map_timer();

  // Start GPS parser
  pthread_t gps_thread;
  pthread_create(&gps_thread, NULL, gps_parser, NULL);

  // Start accelerometer reader
  pthread_t accelerometer_thread;
  pthread_create(&accelerometer_thread, NULL, accelerometer_reader, NULL);

  // Start reed reader
  pthread_t reed_thread;
  pthread_create(&reed_thread, NULL, reed, NULL);

  // Start velocity logger
  pthread_t velocity_logger_thread;
  pthread_create(&velocity_logger_thread, NULL, velocity_logger, NULL);

  // Main loop
  while (1) continue;

  return 0;
}
