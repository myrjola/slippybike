# Slippybike #

Slip detection for a bicycle.

## Quick start ##

Run `make` to compile the `slippybike` binary.

## Licensing ##

The whole project is licensed under the MIT License with the exception of the
minmea.c and minmea.h files that use the license in
https://github.com/cloudyourcar/minmea/blob/master/COPYING and the ADXL345
library that is copyrighted to Analog Devices.

## Acknowledgments ## 

This project makes use of the Minmea library from
https://github.com/cloudyourcar/minmea originally created by Kosma Moczek.
