# Copyright (c) 2016 Slippybike team

CFLAGS += -g -Wall -Wextra -std=c99
CFLAGS += -D_POSIX_C_SOURCE=199309L -D_BSD_SOURCE -D_DEFAULT_SOURCE -D_DARWIN_C_SOURCE
LDLIBS = -l pthread -l bcm2835

all: slippybike

clean:
	$(RM) slippybike *.o adxl345/*.o

slippybike: slippybike.o slip.c gps.c reed.o minmea.o accelerometer.o adxl345/ADXL345.o adxl345/Communication.o velocitylogger.o map_timer.o

.PHONY: all clean
