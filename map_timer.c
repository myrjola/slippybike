// Based on the code from
// https://mindplusplus.wordpress.com/2013/05/21/accessing-the-raspberry-pis-1mhz-timer/

#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// This offset is for the Raspberry Pi 3
#define ST_BASE (0x3F003000)
#define TIMER_OFFSET (4)

// Maps the system timer on the RPi board so that we get a better microsecond
// timer than the alternatives. It returns a pointer to a 64-bit integer
// containing the current timer value. It's a 1 MHz timer, so every tick is a
// microsecond.
long long int *map_timer(void) {
    long long int *timer; // 64 bit timer
    int fd;
    void *st_base; // byte ptr to simplify offset math

    // get access to system core memory
    if (-1 == (fd = open("/dev/mem", O_RDONLY))) {
        fprintf(stderr, "open() failed.\n");
        return NULL;
    }

    // map a specific page into process's address space
    if (MAP_FAILED == (st_base = mmap(NULL, 4096,
                        PROT_READ, MAP_SHARED, fd, ST_BASE))) {
        fprintf(stderr, "mmap() failed.\n");
        return NULL;
    }

    // set up pointer, based on mapped page
    timer = (long long int *)((char *)st_base + TIMER_OFFSET);
    return timer;
}
